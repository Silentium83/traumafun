from datetime import datetime, timedelta, timezone

from django.core.management import BaseCommand
from django.db import transaction
from mastodon import Mastodon

from ... import config as c
from ...models import Toot


class Command(BaseCommand):
    help = 'fetch all new posts, while respecting rate limits. might take a while.'

    def add_arguments(self, parser):
        parser.add_argument(
            '--rebuild-cache',
            action='store_true',
            help='clear stored toots before fetching them again'
        )

    def handle(self, *args, **options):
        max_runtime = c.MAX_REBUILD_RUNTIME if options['rebuild_cache'] else c.MAX_FETCH_RUNTIME
        end_after = datetime.now(timezone.utc) + timedelta(seconds=max_runtime)
        m = Mastodon(
            access_token=c.USERCRED_FILE
        )

        if c.IGNORE_SELF:
            c.IGNORE_IDS.append(m.me()['id'])

        follows = m.account_following(m.me())

        if options['rebuild_cache']:
            recent_known_ids = []
        else:
            recent_known_ids = []
            for user in follows:
                recent_known_ids.extend([t.toot_id for t in Toot.objects.order_by('-toot_id').filter(author=user['id'])[:10]])

        toots = []
        n = 1

        for user in follows:
            self.stdout.write(f'fetching posts from "{user["display_name"]}"')
            page = m.account_statuses(user)

            while True:
                if page is None or len(page) == 0:
                    break

                print('.', end='', flush=True)
                n += 1
                brek = False

                for t in page:
                    if t['reblog'] or t['mentions'] or t['poll'] or t['media_attachments']:
                        continue
                    if t['account']['id'] in c.IGNORE_IDS:
                        continue
                    if any([kw in t['content'] for kw in c.IGNORE_KEYWORDS]):
                        continue
                    if t['id'] in recent_known_ids:
                        brek = True
                        break

                    top_of_thread = not bool(t['in_reply_to_id'])

                    toots.append(Toot(toot_id=t['id'],
                                      author=t['account']['id'],
                                      spoiler_text=t['spoiler_text'],
                                      content=t['content'],
                                      creation_date=t['created_at'],
                                      is_top_of_thread=top_of_thread,
                                      id_parent=t['in_reply_to_id']))

                if brek:
                    break

                if datetime.now(timezone.utc) > end_after:
                    break

                page = m.fetch_next(page)

            self.stdout.write('')

            if datetime.now(timezone.utc) > end_after:
                self.stdout.write(self.style.NOTICE(f'hit runtime limit ({max_runtime} seconds), stopping'))
                break

        if options['rebuild_cache']:
            self.stdout.write(self.style.NOTICE('wiping all toot objects'))
            Toot.objects.all().delete()

        Toot.objects.bulk_create(toots)

        self.stdout.write(self.style.SUCCESS('created toot objects'))

        with transaction.atomic():
            for toot in Toot.objects.filter(is_top_of_thread=False):
                toot.link_parent()
                toot.save()

        self.stdout.write(self.style.SUCCESS('linked threads'))
