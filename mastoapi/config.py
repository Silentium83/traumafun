# either put your values here, or put code here that loads them from e.g. environment variables, depending on deployment

INSTANCE_URL = ''
EMAIL = ''
PASSWORD = ''
CLIENT_NAME = ''

IGNORE_IDS = []
IGNORE_SELF = True  # automatically adds the bot's id into ignore_ids
IGNORE_KEYWORDS = ["!notfb"]

CLIENTCRED_FILE = 'clientcred.secret'
USERCRED_FILE = 'usercred.secret'

# fetchposts will only run for the set amount of time (in seconds) or less
MAX_FETCH_RUNTIME = 5 * 60
MAX_REBUILD_RUNTIME = 45 * 60

VERIFICATION_TOOT = False
VERIFICATION_TOOT_TEXT = 'successfully logged in and got an access token'
