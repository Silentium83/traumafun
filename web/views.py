from django.shortcuts import render

# Create your views here.
from django.views.generic import TemplateView

from mastoapi.models import Toot


class HomeView(TemplateView):
    template_name = 'traumafun/home.html'


class RandomPostView(TemplateView):
    template_name = 'traumafun/post.html'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        t = Toot.objects.filter(is_top_of_thread=True).order_by('?')[0]
        t.depth = 0
        toots = [t]
        children = self.recurse(t)
        if children is not None:
            toots.extend(children)
        ctx['toot'] = toots
        return ctx

    @classmethod
    def recurse(cls, toot, r=1):
        children = Toot.objects.filter(parent=toot).order_by('creation_date')
        if len(children) > 0:
            o = []
            for child in children:
                child.depth = r
                o.append(child)
                c = cls.recurse(child, r+1)
                if c is not None:
                    o.extend(c)
            return o
        else:
            return None
