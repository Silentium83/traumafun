from django.db import models

# Create your models here.


class Toot(models.Model):
    toot_id = models.BigIntegerField(unique=True)
    author = models.BigIntegerField()
    spoiler_text = models.TextField(null=True)
    content = models.TextField()
    creation_date = models.DateTimeField()

    is_top_of_thread = models.BooleanField(default=False)
    orphaned = models.BooleanField(default=False)
    id_parent = models.BigIntegerField(null=True)
    parent = models.ForeignKey('Toot', on_delete=models.CASCADE, null=True)

    def link_parent(self):
        if not self.is_top_of_thread and not self.orphaned:
            try:
                self.parent = Toot.objects.get(toot_id=self.id_parent)
            except Toot.DoesNotExist:
                self.orphaned = True
