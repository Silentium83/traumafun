from django.urls import path

from web.views import RandomPostView, HomeView

urlpatterns = [
    path('', HomeView.as_view(), name='home'),
    path('post/', RandomPostView.as_view(), name='random')
]