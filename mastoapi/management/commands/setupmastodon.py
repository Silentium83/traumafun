from os.path import exists

from django.core.management import BaseCommand, CommandError
from mastodon import Mastodon, MastodonNetworkError, MastodonAPIError, MastodonIllegalArgumentError
from ... import config as c


class Command(BaseCommand):
    help = 'sets up mastodon credentials and saves them into a file'

    def handle(self, *a, **opts):
        try:
            Mastodon.create_app(
                c.CLIENT_NAME,
                api_base_url=c.INSTANCE_URL,
                to_file=c.CLIENTCRED_FILE
            )
            self.stdout.write(self.style.SUCCESS('created app'))
        except MastodonNetworkError:
            raise CommandError('error while creating the app, check your CLIENT_NAME and INSTANCE_URL')

        m = Mastodon(
            client_id=c.CLIENTCRED_FILE
        )

        try:
            m.log_in(
                c.EMAIL,
                c.PASSWORD,
                to_file=c.USERCRED_FILE
            )
            self.stdout.write(self.style.SUCCESS('logged in'))
        except MastodonIllegalArgumentError as e:
            raise CommandError(f'IllegalArgumentError while logging in: {e}')
        except MastodonAPIError as e:
            raise CommandError(f'APIError while logging in: {e}')

        if c.VERIFICATION_TOOT:
            try:
                m.toot(c.VERIFICATION_TOOT_TEXT)
                self.stdout.write(self.style.SUCCESS('sent verification toot'))
            except MastodonIllegalArgumentError as e:
                raise CommandError(f'IllegalArgumentError while posting verification toot: {e}')
            except ValueError as e:
                raise CommandError(f'ValueError while posting verification toot: {e}')
