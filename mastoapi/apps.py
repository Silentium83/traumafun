from django.apps import AppConfig


class MastoapiConfig(AppConfig):
    name = 'mastoapi'
